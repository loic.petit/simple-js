const path = require('path');

const webpack = require('webpack');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = function(env) { return {
    context: __dirname,
    mode: 'development',
    entry: './app/app.js',
    output: {
        filename: "[name].js?[hash]",
        chunkFilename: "[name].js?[hash]",
        path: __dirname + "/dist"
    },
    devServer: {
        compress: true,
        port: 9000,
        proxy: {
            '/api/**': {
                target: 'http://localhost:8080',
                secure: false
            },
        },
        historyApiFallback: {
          rewrites: [
            { from: /^\/.*/, to: '/index.html' },
          ]
        }
    },
    plugins: [
        new webpack.DefinePlugin({
            ON_PROD: env.prod
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        })
    ],
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true // set to true if you want JS source maps
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                loader: 'raw-loader'
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'postcss-loader', // Run post css actions
                        options: {
                            plugins: function () { // post css plugins, can be exported to postcss.config.js
                                return [
                                    require('precss'),
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    "sass-loader"
                ]
            },
            {
                test: /\.(gif|jpeg|jpg|png|ico|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader?name=[path][name].[ext]?[hash]"
            }
        ]

    }
}};
