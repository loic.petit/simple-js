# simple-js

My repo for doing pure ECMA 6 + SASS operations using a backend api without anything more complex.

## Install

You know npm...

## What it does

Compiles app/*.js and dependencies into dist/main.js

Compiles app/*.sass and dependencies into dist/main.css

Leaves dist/index.html and other things alone.

Deliver /api by proxifying localhost:8080/api

Deliver dist at localhost:9000, any 404 is redirected to index.html for html5 routing purposes.

## Run

```npm run run```

Target main.js / main.css are compiled and can be debugged and traced in debugger.

## Dist

```npm run dist```

Target main.js / main.css are compressed for production.
